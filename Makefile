GIT_COMMIT := $(shell git rev-parse --short HEAD)
images:
	docker build csv-service -t "ghcr.io/shikachuu/csv-service:$(GIT_COMMIT)"
	docker tag "registry.gitlab.com/shikachuu/utility-services/csv-service:$(GIT_COMMIT)" "registry.gitlab.com/shikachuu/utility-services/csv-service:latest"

	docker build -f src/main/docker/Dockerfile mailer-service -t "registry.gitlab.com/shikachuu/utility-services/mailer-service:$(GIT_COMMIT)"
    docker tag "registry.gitlab.com/shikachuu/utility-services/mailer-service:$(GIT_COMMIT)" "registry.gitlab.com/shikachuu/utility-services/mailer-service:latest"