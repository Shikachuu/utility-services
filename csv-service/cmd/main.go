package main

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-kit/log"
	"gitlab.com/Shikachuu/utility-services/csv-service/pkg"
)

func main() {
	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)

	_ = logger.Log("level", "info", "msg", "service started")

	if err := run(logger); err != nil && errors.Is(err, http.ErrServerClosed) == false {
		_ = logger.Log("level", "error", "msg", "encountered an error", "error", err)
		os.Exit(1)
	} else {
		_ = logger.Log("level", "info", "msg", "shutting down gracefully")
	}
}

func run(logger log.Logger) error {
	router := chi.NewRouter()
	srv := pkg.NewServer(router, logger)

	httpSrv := &http.Server{
		Handler:      srv,
		Addr:         "0.0.0.0:8080",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt, syscall.SIGINT)
		<-sigint

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		if err := httpSrv.Shutdown(ctx); err != nil {
			_ = logger.Log("level", "error", "msg", "encountered an error", "error", err)
		}
	}()
	return httpSrv.ListenAndServe()
}
