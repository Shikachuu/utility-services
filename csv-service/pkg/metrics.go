package pkg

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type serverMetrics struct {
	bytesProcessed prometheus.Histogram
}

func (s *Server) registerMetrics() {
	s.registry.MustRegister(collectors.NewBuildInfoCollector())
	s.registry.MustRegister(collectors.NewGoCollector(collectors.WithGoCollections(collectors.GoRuntimeMetricsCollection)))

	s.metrics.bytesProcessed = promauto.NewHistogram(prometheus.HistogramOpts{
		Namespace: "csvservice",
		Name:      "processed_csv_bytes",
		Help:      "Processed CSV byte size",
		Buckets:   []float64{100.0, 1000.0, 10000.0, 50000.0, 100000.0, 500000.0, 1000000.0},
	})
	s.registry.MustRegister(s.metrics.bytesProcessed)
}
