package pkg

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"time"
)

func (s *Server) routes() {
	s.router.Handle("/metrics", promhttp.HandlerFor(
		s.registry,
		promhttp.HandlerOpts{
			Timeout:           15 * time.Second,
			EnableOpenMetrics: true,
		},
	))
	s.router.Get("/health", s.newHealthHandler())
	s.router.Post("/csv-service", s.newCSVServiceHandler())
}
