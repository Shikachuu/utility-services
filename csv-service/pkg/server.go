package pkg

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-kit/log"
	"github.com/prometheus/client_golang/prometheus"
	"net/http"
)

type Server struct {
	router   *chi.Mux
	logger   log.Logger
	registry *prometheus.Registry
	metrics  serverMetrics
}

func NewServer(router *chi.Mux, logger log.Logger) *Server {
	s := &Server{
		router:   router,
		logger:   logger,
		registry: prometheus.NewRegistry(),
	}
	s.routes()
	s.registerMetrics()
	return s
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
