# mailer-service
## Env vars:
- `QUARKUS_MAILER_FROM` **string** - alias for the from paramter
- `QUARKUS_MAILER_HOST` **string** - smtp server address
- `QUARKUS_MAILER_PORT` **int** - smtp port
- `QUARKUS_MAILER_SSL` **bool** - use ssl while connecting smtp
- `QUARKUS_MAILER_USERNAME` **string** - smtp username
- `QUARKUS_MAILER_PASSWORD` **string** - smtp password
- `QUARKUS_MAILER_MOCK` **bool** - if turned on write emails to log instead of sending them
