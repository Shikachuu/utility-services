import org.jboss.resteasy.annotations.providers.multipart.PartType
import java.io.File
import javax.ws.rs.FormParam
import javax.ws.rs.core.MediaType

class Content {
    @FormParam("file")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    lateinit var file: File

    @FormParam("target")
    @PartType(MediaType.TEXT_PLAIN)
    lateinit var target: String
}
