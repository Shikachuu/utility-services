import io.quarkus.mailer.Mail
import io.quarkus.mailer.Mailer
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm
import java.nio.file.Files
import javax.inject.Inject
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/send")
class MailerResource {
    @Inject
    lateinit var mailer: Mailer

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    fun send(@MultipartForm content: Content): Response {
        mailer.send(
            Mail.withText(
                content.target,
                "Mailer ",
                "Hello,\n there is new attachment sent to you by the mailer service"
            )
                .addAttachment(content.file.name, content.file, Files.probeContentType(content.file.toPath()))
        )

        return Response.ok().build()
    }
}